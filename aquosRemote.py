#!/usr/bin/env python3

import socket

class aquosRemote:

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def setVolume(self, volume):
        if (volume > 50 | volume < 0):
            print("Volume must be between 0 and 50")

        volumeString = "{:02d}".format(volume)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port))
            s.sendall(b'VOLM' + bytes(volumeString, "ascii") + b'  \x0D')
            s.close()
        
    def setInput(self, input):
        if (input > 9 | input < 1):
            print("Input must be between 1 and 9")

        inputString = str(input)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port))
            s.sendall(b'IAVD' + bytes(inputString, "ascii") + b'   \x0D')
            s.close()

    def powerOff(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port))
            s.sendall(b'POWR0   \x0D')
            s.close()