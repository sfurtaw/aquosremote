#!/usr/bin/env python3

import aquosRemote
import time

DELAY = 4
remote = aquosRemote.aquosRemote("10.0.1.3", 10002)

remote.setVolume(20)
time.sleep(DELAY)
remote.setVolume(0)
time.sleep(DELAY)
remote.setVolume(11)
time.sleep(DELAY)
remote.setVolume(20)
time.sleep(DELAY)
remote.setInput(4)
time.sleep(DELAY)
remote.setInput(1)
time.sleep(DELAY)
remote.powerOff()